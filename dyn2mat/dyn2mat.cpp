// dyn2mat.cpp : Defines the entry point for the console application.
// Anywhere it tells you to refer to REFn, where n is an integer, search for [REFn] to find the comment or line of code to which it refers.

#include "stdafx.h"
#include <regex>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <limits>
using namespace std;
using namespace std::tr1;

char* catbuffer=(char*)malloc(sizeof(char)*1024); //Used to stick the names of required files onto the end of the command-line provided paths
const int flagsc=3; // The number of flags
const char* flagsn[flagsc]={"-?","-q","-v"}; // The names of the flags; if you add one, make sure you increment flagsc and add it to the help text at REF1.
bool flagsv[flagsc]={false}; //The values of each; true (included) or false. Do not change this line if you add more.


void openfile(ifstream* f,char*name,char*loc,char*forWhat) { //[REF0]
	strcpy(catbuffer,loc); //Set catbuffer to the DynusT output path or MATLAB input path, depending on whether this as I or O file.
	strcat(catbuffer,"\\");
	(*f).open(strcat(catbuffer,name)); // Append the DAT file to path, and open this file into infile
	if (!(*f).is_open()) {
		cout << "Unable to open file\"" << catbuffer << "\" for " << forWhat << "." << std::endl;
		throw"";
	}
}
void openfile(ofstream* f,char*name,char*loc,char*forWhat) { //Overload necessary to handle both I and O.
	strcpy(catbuffer,loc); //Set catbuffer to the DynusT output path or MATLAB input path, depending on whether this as I or O file.
	strcat(catbuffer,"\\");
	(*f).open(strcat(catbuffer,name)); // Append the DAT file to path, and open this file into infile
	if (!(*f).is_open()) {
		cout << "Unable to open file\"" << catbuffer << "\" for " << forWhat << "." << std::endl;
		throw"";
	}
}

int main(int argc, char*argv[]) {
	for(int i=0;i<argc;i++) // For each argument...
		for(int j=0;j<flagsc;j++) //... and each flag, if they're equal => the flag is included, so...
			if (!strcmp((char*)(argv[i]),flagsn[j])) flagsv[j]=true; // Set the corresponding value to true.

	try { //Any input errors will:
		  // - Print the error to screen
		  // - Throw an empty string
		  // Following this, the error is caught and the help info is printed. This is for USER INPUT ERRORS.
		  // Any other error should be either handled within the code, or thrown without an empty string, in which case the error message is printed.

		if (argc<2) { //We need at least the input (1) and output (2) [as well as the exe path (0)!] parameters given... 
			cout << "Both the path to the DynusT output folder and that to the MATLAB folder must be specified as the first two arguments.";
			throw ""; // ...If not throw and tell the user what we need.
		}
		
		if(flagsv[0]) throw ""; // If the user asks for help, give it.

		std::string line; //Read input into this
		int maxnum=std::numeric_limits<int>::max();

		// Open all files we'll need, and ensure that they opened properly.
	    if(flagsv[2]) cout << "  (1/8) Opening files;\n";
		ifstream infile; 
		openfile(&infile,"OutAccuVol.dat",argv[1],"reading traffic data"); //Defined at REF0
		ofstream f; // Used to output data into the MATLAB folder
		openfile(&f,"DynusT_f.dat",argv[2],"writing traffic data");
		ofstream cdat; // Used to output capacity data into the MATLAB folder
		openfile(&cdat,"DynusT_c.dat",argv[2],"writing network capacity data");
		ofstream script; // Used to output the MATLAB script to do post-processing and run the Haoyang Code
		openfile(&script,"DynusT.m",argv[2],"writing the MATLAB script");
		ofstream lnode; // List of nodes
		openfile(&lnode,"DynusT_nl.dat",argv[2],"writing the list of nodes");
		ifstream summstat;
		openfile(&summstat,"SummaryStat.dat",argv[1],"reading network summary data");
		ifstream links;
		openfile(&links,"network.dat",argv[1],"reading network connexion data");
		ofstream h_tout; // Used to output the MATLAB script to do post-processing and run the Haoyang Code
		openfile(&h_tout,"DynusT_h_t.dat",argv[2],"writing the adjacency matrix");
		ifstream lspeed; 
		openfile(&lspeed,"fort.900",argv[1],"reading link-speed data"); 
		ifstream ldensity; 
		openfile(&ldensity,"fort.700",argv[1],"reading link-density data"); 
		ofstream lspeedo; // Used to output data into the MATLAB folder
		openfile(&lspeedo,"DynusT_lspeed.dat",argv[2],"writing traffic data");
		ofstream ldensityo; // Used to output the MATLAB script to do post-processing and run the Haoyang Code
		openfile(&ldensityo,"DynusT_ldensity.dat",argv[2],"writing the MATLAB script");
		ofstream lxy; // Used to ###
		openfile(&lxy,"DynusT_xy.dat",argv[2],"writing the MATLAB script");
		// If you need to add any more, make sure to close them at REF2

		//////////////////////
		// Begin conversion //
		//////////////////////

		////// Read n, m and z from summarystat
		int n,m,z;
		const regex basenetwork(" +Number of (Nodes|Links|Zones) +: +([0-9]+)"); //This regex finds lines with at least two fixed point numbers on it, separated by spaces and with 5 decimal places. I.e. it finds lines with data on them.

	    if(flagsv[2]) cout << "  (2/8) Reading basic data;\n";
		while (summstat.good()) //For each line in the input...
		{
			smatch match;
			getline(summstat,line);
			regex_search(line, match, basenetwork);
			if (!match.empty()) {
				//cout<<match[1]<<"\n";
				switch (((std::string)match[1])[0]) {
				case 'N':
						n=atoi(((string)match[2]).c_str());
				case 'L':
						m=atoi(((string)match[2]).c_str());
				case 'Z':
						z=atoi(((string)match[2]).c_str());
				}
			}
		} 
		////// Create A and save 
		//bool*A=(bool*)calloc(n*n,sizeof(bool)); //REFC
		istringstream buffer(catbuffer);
		int i=0;
		int r,c=0;
		const regex linkline(" +([0-9]+) +([0-9]+) +[0-9]+ +[0-9]+ +[0-9]+ +[0-9]+ +[0-9]+ +(\\+|\\-)[0-9]+ +[0-9]+ +([0-9]+) +[0-9]+ +[0-9]+ +(\\+|\\-)[0-9]+"); //This regex finds lines with at least two fixed point numbers on it, separated by spaces and with 5 decimal places. I.e. it finds lines with data on them.
		const regex nodeline(" +([0-9]+) +([0-9]+)"); //
	    if(flagsv[2]) cout << "  (3/8) Reading link info: writing network and capacity data;\n";
//#pragma omp while
		smatch match;
		while (links.good()) //For each line in the input...
		{
			getline(links,line);
			if(i>0&&i<=n) {
				regex_search(line, match, nodeline);
				//lxy << match[1] << "\n";
				lnode << match[1] << "\n";
			} else if (i>n&&i<=n+m) {
				regex_search(line, match, linkline);
				if (!match.empty()) {
//#pragma omp ordered {
					buffer=istringstream(match[1]);
					buffer >> r;
					buffer=istringstream(match[2]);
					buffer >> c;
					cdat << match[4];
					if(i<n+m) cdat << "\t";
					h_tout << c << "\t" << r << "\n";
				//}
					//A[(r-1)*n+c-1]=true; //REFC
					//cout << r<<","<<c << "\n";
				} else if(line.length()) {
					throw "Malformed network.dat file.";
				}
			}
			i++;
		}
		// Output A //REFC
		/*for(r=0;r<n;r++) {
			for(c=0;c<n;c++)
				aout<<A[r*n+c]<<" ";
			aout<<"\n";
		}
		free(A);*/

		////// Load traffic data
		int num=0;
		regex iswhsp("^[ \t]*$"); //This regex finds lines with at least two fixed point numbers on it, separated by spaces and with 1+ decimal places. I.e. it finds lines with data on them.
		regex onedatum(" *([0-9]+.[0-9]+)"); //This regex finds lines with at least two fixed point numbers on it, separated by spaces and with 1+ decimal places. I.e. it finds lines with data on them.
	    if(flagsv[2]) cout << "  (4/8) Converting traffic data;\n";
	    if(flagsv[3]) cout << "________________________________________\n";
		for(int k=0;k<5;k++)
			getline(infile,line);
		bool waswhsp=true;
		while (infile.good()&&num<maxnum) //For each line in the input...
		{
			getline(infile,line);

			//cout<<regex_match(line,iswhsp);
			if(regex_match(line,iswhsp)) {
				if(!waswhsp) f << "\n";
				waswhsp=true;
				continue;
			}
			if(waswhsp) {
				num++;
				waswhsp=false;
				continue;
			}
			waswhsp=false;

			std::tr1::sregex_iterator first(line.begin(), line.end(), onedatum);
			std::tr1::sregex_iterator last;

			for (auto i = first; i != last; i++) 
			{
				f << "" << i->str(0);
			}
		}
		////// Convert density data
		num=0;
	    if(flagsv[2]) cout << "  (5/8) Converting speed data;\n";
	    if(flagsv[3]) cout << "________________________________________\n";
		for(int k=0;k<5;k++)
			getline(lspeed,line);
		waswhsp=true;
		while (lspeed.good()&&num<maxnum) //For each line in the input...
		{
			getline(lspeed,line);

			//cout<<regex_match(line,iswhsp);
			if(regex_match(line,iswhsp)) {
				if(!waswhsp) lspeedo << "\n";
				waswhsp=true;
				continue;
			}
			if(waswhsp) {
				num++;
				waswhsp=false;
				continue;
			}
			waswhsp=false;

			std::tr1::sregex_iterator first(line.begin(), line.end(), onedatum);
			std::tr1::sregex_iterator last;

			for (auto i = first; i != last; i++) 
			{
				lspeedo << "" << i->str(0);
			}
		}
		////// Convert density data
		num=0;
	    if(flagsv[2]) cout << "  (6/8) Converting density data;\n";
	    if(flagsv[3]) cout << "________________________________________\n";
		for(int k=0;k<5;k++)
			getline(ldensity,line);
		waswhsp=true;
		while (ldensity.good()&&num<maxnum) //For each line in the input...
		{
			getline(ldensity,line);

			//cout<<regex_match(line,iswhsp);
			if(regex_match(line,iswhsp)) {
				if(!waswhsp) ldensityo << "\n";
				waswhsp=true;
				continue;
			}
			if(waswhsp) {
				num++;
				waswhsp=false;
				continue;
			}
			waswhsp=false;

			std::tr1::sregex_iterator first(line.begin(), line.end(), onedatum);
			std::tr1::sregex_iterator last;

			for (auto i = first; i != last; i++) 
			{
				ldensityo << "" << i->str(0);
			}
		}
		/*
		////// Convert speed data
		num=0;
	    if(flagsv[2]) cout << "  (5/8) Converting speed data;\n";
		while (lspeed.good()&&num<maxnum) //For each line in the input...
		{
			getline(lspeed,line);
			std::tr1::sregex_iterator first(line.begin(), line.end(), istraffic);
			std::tr1::sregex_iterator last;

			for (auto i = first; i != last; i++) 
			{
				lspeedo << "" << i->str(0) << "\n";
				num++;
			}
		}

		////// Load density data
		num=0;
	    if(flagsv[2]) cout << "  (6/8) Converting density data;\n";
		while (ldensity.good()&&num<maxnum) //For each line in the input...
		{
			getline(ldensity,line);
			std::tr1::sregex_iterator first(line.begin(), line.end(), istraffic);
			std::tr1::sregex_iterator last;

			for (auto i = first; i != last; i++) 
			{
				ldensityo << "" << i->str(0) << "\n";
				num++;
			}
		}
		*/
		////////////////////
		// End conversion //
		// Begin scripts  //
//		////////////////////
			    if(flagsv[2]) cout << "  (7/8) Outputting scripts;\n";
				script << "c=textread('DynusT_c.dat');\n";
				script << "f=textread('DynusT_f.dat');\n";
				//script << "A=textread('DynusT_A.dat');\n"; //REFC
				script << "h_t=textread('DynusT_h_t.dat');\n"; //"Head_Tail_Nd_Pos(B0, n, m);\n";
				script << "nl=textread('DynusT_nl.dat');\n";
				script << "n="<<n<<";\n";
				script << "m="<<m<<";\n";
				script << "z="<<z<<";\n";
				script << "B0=[]; %Head_Tail_Nd_Pos_Inv(h_t,n,m);\n"; //Incd_from_Adj_Mtx(A);\n";
				script << "v=textread('DynusT_lspeed.dat'); %veh/mile/lane\n";
				script << "d=textread('DynusT_ldensity.dat'); %mile/h\n";
				script << "q=v.*d; %veh/h/lane\n";
				script << "nt=size(d,1); %Number of time intervals\n";
				script << "nodef=fopen('" << argv[1] << "\\node.csv');\n";
				script << "textscan(nodef,'%s %s %s',1, 'delimiter',',');\n";
				script << "nodedata=textscan(nodef,'%d %d %d','delimiter',',');\n";
				script << "fclose(nodef);\n";
				script << "ll=zeros(1,m);\n";
				script << "s=zeros(2,2);\n";
				script << "for i=1:m\n";
				script << "    s(1,1)=nodedata{1,2}(find(nodedata{1,1}==h_t(i,1)));\n";
				script << "    s(1,2)=nodedata{1,3}(find(nodedata{1,1}==h_t(i,1)));\n";
				script << "    s(2,1)=nodedata{1,2}(find(nodedata{1,1}==h_t(i,2)));\n";
				script << "    s(2,2)=nodedata{1,3}(find(nodedata{1,1}==h_t(i,2)));\n";
				script << "    ll(1,i)=sqrt((s(1,1)-s(2,1))^2+(s(2,1)-s(2,2))^2);\n";
				script << "end\n";
				script << "ll=ll(ones(220,1),:);\n";
				script << "t=ll2./v;\n";
				script << "save('preparation_for_PS.mat','n','m','h_t','B0');\n";
  				script << "save('data_up_to_countingcars_single_edge_0_0.mat','f','c','v','d','q');\n";
				script << "save('data_up_to_countingcars_single_edge_1_0.mat','f','c','v','d','q');\n";
				script << "save('data_up_to_countingcars_single_edge_2_0.mat','f','c','v','d','q');\n";
				script << "save('dyn_oct.mat','f','c','v','d','h_t','nl','n','m','z','nt','t');\n";

				//mat2oct<< "";
		/////////////////
		// End scripts //
		/////////////////

		//[REF2] Close all files.
		if(flagsv[2]) cout << "  (8/8) Closing output files;\n";
		infile.close();
		f.close();
		cdat.close();
		script.close();
		lnode.close();
		summstat.close();
		links.close();
		h_tout.close();
		lspeed.close();
		ldensity.close();
		lspeedo.close();
		ldensityo.close();
		lxy.close();

		if(!flagsv[1]) cout<<"Completed. Run DynusT.m to run\n";
	} catch(char* error) {
		if(strlen(error)) {
			cout<<"Error: " << error << "\n";
		} else 
			//The below text is dumped to the console when the user uses -? or an incorrect argument. [REF1]
			std::cout<<"This program converts DynusT output to a format readable by the MATLAB program, Haoyang Code;\nIt reads in the files created by DynusT, and outputs those needed by MATLAB; specify the location of the DynusT files in the first argument, and that of the MATLAB folder as the second argument. Do not include a trailing slash.\nThe syntax, then, is:\ndyn2mat \"DynusT path\" \"MATLAB Path\" [Flags]\nWhere flags can be any of:\n\t-?: Show help\n\t-q: Quiet run; output only on errors\n\t-v: Verbose \n\n";
	}
    if(!flagsv[1]) system("pause");
	return 0;
}

